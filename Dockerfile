FROM jenkinsci/blueocean

USER root

RUN apk update && apk add make python-dev libffi-dev openssl-dev gcc libc-dev py-pip && \
    pip install docker-compose

VOLUME /var/run/docker.sock
